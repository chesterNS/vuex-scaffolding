import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/pages/Dashboard'
import Main from '@/pages/Main'
import Product from '@/pages/Product'
import Brand from '@/pages/Brand'
import Login from '@/pages/Login'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/product',
      name: 'product',
      component: Product
    },
    {
      path: '/brand',
      name: 'brand',
      component: Brand
    },
  ]
})
